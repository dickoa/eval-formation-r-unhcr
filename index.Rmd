---
title: "Evaluation de la formation introductive à R"
subtitle: "Evaluation de la formation introductive à R"
author: "DIMA West and Central Africa"
date: " `r format(Sys.Date(),  '%d %B %Y')`"
always_allow_html: yes
output:
  unhcRstyle::unhcr_templ_html:
    toc: true
---


```{r setup, include = FALSE, message = FALSE, warning = FALSE}
knitr::opts_chunk$set(echo = FALSE,
                      message = FALSE,
                      warning = FALSE,
                      collapse = FALSE,
                      comment = "#>",
                      fig.align = "center")
knitr::opts_chunk$set(fig.width = 12, fig.height = 12)
set.seed(1)
extrafont::loadfonts(quiet = TRUE)
options(scipen = 999) # turn-off scientific notation like 1e+48
library(tidyverse)
library(robotoolbox)
library(unhcRstyle)
library(reactable)

stat_mode <- function(x) {
  tb <- table(x)
  names(tb)[which.max(tb)]
}

kobo_setup(url = "https://kobo.unhcr.org",
           token = Sys.getenv("KOBOTOOLBOX_TOKEN"))

uid <- kobo_asset_list() |>
  filter(str_detect(name, regex("evaluation formation R",
                                ignore_case = TRUE))) |>
  pull(uid) |>
  first()

asset <- kobo_asset(uid)
data <- kobo_submissions(asset)
form <- kobo_form(asset)

lvl1 <- form$choices |>
  filter(list_name == "module_satisf") |>
  pull(name)

lvl2 <- form$choices |>
  filter(list_name == "jour") |>
  pull(name)

lvl3 <- form$choices |>
  filter(list_name == "accord") |>
  pull(name)
```

# Note globale de la formation

`r nrow(data)` personnes ont soumis une note globale à la formation. La majorité des participants ont donné une note de `r stat_mode(data$feedback6_note)` sur 10. La note la plus faible est de `r min(data$feedback6_note, na.rm = TRUE)` et la plus élévée `r max(data$feedback6_note, na.rm = TRUE)`.

```{r, fig.height = 9}
data |>
  select(feedback6_note) |>
  count(feedback6_note) |>
  mutate(p = n / sum(n)) |>
  ggplot(aes(x = feedback6_note, y = p)) +
  geom_col(fill = unhcr_blue) +
  geom_text(aes(label = paste0(round(100 * p, 1), "%")),
            fontface = "bold",
            size = 7,
            vjust = 1.3,
            color = "#FFFFFF") +
  geom_text(data = tibble(x = 1:10, y = 0, label = 1:10),
            aes(x = x, y = y, label = label),
            vjust = 1.5,
            size = 8,
            fontface = "bold") +
  scale_x_continuous(limits = c(1, 11),
                     breaks = 1:10,
                     labels = 1:10) +
  labs(x = "", y = "") +
  unhcr_theme(grid = "") +
  theme(axis.text.x = element_blank(),
        axis.text.y = element_blank())
```

# Adéquation des modules


```{r, fig.height = 16}
data |>
  select(starts_with("feedback4")) |>
  pivot_longer(cols = everything()) |>
  mutate(name = str_remove_all(name, "feedback4_module_"),
         value = factor(value, levels = lvl1, ordered = TRUE)) |>
  count(name, value, .drop = FALSE) |>
  group_by(name) |>
  mutate(p = n / sum(n)) |>
  ungroup() |>
  ggplot() +
  geom_col(aes(value, p),
           fill = unhcr_blue) +
  geom_text(aes(value, p, label = paste0(round(100 * p, 1), "%")),
            vjust = 1.2,
            color = "white",
            size = 7,
            fontface = "bold") +
  facet_wrap(vars(name), ncol = 1) +
  labs(x = "", y = "") +
  unhcr_theme(grid = "") +
  theme(axis.text.y = element_blank(),
        strip.text = element_text(size = 16, face = "bold"))
```

# Précisez les modifications

```{r}
data |>
  select(starts_with("precisez")) |>
  pivot_longer(cols = everything(),
               names_to = "module",
               values_to = "precision") |>
  mutate(module = str_remove_all(module, "precisez_")) |>
  arrange(module) |>
  drop_na() |>
  reactable(filterable = TRUE, searchable = TRUE)
```


# Evaluation de la formation


```{r}
data |>
  select(feedback1_objectif, feedback2_eval,
         feedback3_clarte, feedback5_particip) |>
  pivot_longer(cols = everything()) |>
  mutate(name = str_remove_all(name, "feedback\\d_"),
         value = factor(value, levels = lvl3, ordered = TRUE),
         name = factor(name, levels = sort(unique(name), decreasing = TRUE),
                       ordered = TRUE)) |>
  count(name, value, .drop = FALSE) |>
  group_by(name) |>
  mutate(p = n / sum(n)) |>
  ungroup() |>
  ggplot() +
  geom_col(aes(value, p),
           fill = unhcr_blue) +
  geom_text(aes(value, p, label = paste0(round(100 * p, 1), "%")),
            vjust = 1.2,
            color = "white",
            size = 7,
            fontface = "bold") +
  facet_wrap(vars(name), ncol = 1) +
  labs(x = "", y = "") +
  unhcr_theme(grid = "") +
  theme(axis.text.y = element_blank(),
        strip.text = element_text(size = 16, face = "bold"))
```

# Disponibilité pour des formations futures

```{r}
data |>
  select(contains("_jour_")) |>
  pivot_longer(cols = everything()) |>
  mutate(name = str_remove_all(name, "feedback8_dispo_jour_"),
         name = str_replace_all(name, "_", "-"),
         value = factor(value, levels = lvl2, ordered = TRUE),
         name = factor(name, levels = sort(unique(name), decreasing = TRUE),
                       ordered = TRUE)) |>
  count(name, value, .drop = FALSE) |>
  drop_na() |>
  ggplot() +
  geom_tile(aes(x = value, y = name, fill = n),
            color = "black") +
  geom_text(aes(x = value, y = name, label = n),
            size = 8,
            fontface = "bold") +
  scale_fill_gradient(low = "white", high = "steelblue") +
  labs(x = "", y = "") +
  unhcr_theme(grid = "") +
  theme(legend.position = "none",
        axis.text.y = element_text(size = 16),
        axis.text.x = element_text(size = 16))
```


# Commentaires généraux

```{r}
data |>
  select(commentaire = feedback9_commentaire) |>
  drop_na() |>
  reactable(filterable = TRUE, searchable = TRUE)
```
